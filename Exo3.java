package tme2;

import java.util.ArrayList;
import java.util.Random;


public class Exo3 {
	static Random r = new Random();

	public static ArrayList<Noeud> algo_Remy(int size) {

		ArrayList<Noeud> remy_tree = new ArrayList<Noeud>();
		
		// Cas de base 1 seul noeud
		if (size > 0) {
			Noeud n1 = new Noeud(-1, -1, -1, 1);
			remy_tree.add(n1);
		}
		
		int valeur_etiq = 2;
		
		//
		for (int i = 1; i < size; i++) {
			int index_chosen_node = (r.nextInt(remy_tree.size()));

			int pile_face = r.nextInt(2);

			Noeud nouveau_pere;
			Noeud nouveau_fils;
			
			if (pile_face == 0) {

				nouveau_pere = new Noeud(remy_tree.get(index_chosen_node).getPere(), index_chosen_node, -1, -1);
				remy_tree.add(nouveau_pere);
				nouveau_fils = new Noeud(remy_tree.indexOf(nouveau_pere), -1, -1, valeur_etiq++);
				remy_tree.add(nouveau_fils);
				nouveau_pere.setFd(remy_tree.indexOf(nouveau_fils));
				remy_tree.get(index_chosen_node).setPere(remy_tree.indexOf(nouveau_pere));

			} else { 

				nouveau_pere = new Noeud(remy_tree.get(index_chosen_node).getPere(), -1, index_chosen_node, -1);
				remy_tree.add(nouveau_pere);
				nouveau_fils = new Noeud(remy_tree.indexOf(nouveau_pere), -1, -1, valeur_etiq++);
				remy_tree.add(nouveau_fils);
				nouveau_pere.setFg(remy_tree.indexOf(nouveau_fils));
				remy_tree.get(index_chosen_node).setPere(remy_tree.indexOf(nouveau_pere));
			}

		}
		return remy_tree;
	}

	private static class  Noeud {
		private int pere;
		private int fg;
		private int fd;
		private int valeur;

		public Noeud(int pere, int fg, int fd, int val) {
			super();
			this.pere = pere;
			this.fg = fg;
			this.fd = fd;
			this.valeur = val;
		}

		public int getPere() {
			return pere;
		}

		public void setPere(int pere) {
			this.pere = pere;
		}

		public int getFg() {
			return fg;
		}

		public void setFg(int fg) {
			this.fg = fg;
		}

		public int getFd() {
			return fd;
		}

		public void setFd(int fd) {
			this.fd = fd;
		}

		public int getVal() {
			return valeur;
		}

		public void setVal(int val) {
			this.valeur = val;
		}
		
		@Override
		public String toString() {
			return "Noeud (pere=" + pere + ", fg=" + fg + ", fd=" + fd + ", val=" + valeur + ")\n";
		}
	}
	
	public static void main(String[] args) {
		//génération d'un arbre de remy
		ArrayList<Noeud> tree = algo_Remy(1000);
		//affichage de l'arbre de remy
		System.out.println(tree.toString());
		
	}


}

